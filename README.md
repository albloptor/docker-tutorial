Tutorial from Docker website

------
Part 2
------

1. Create Dockerfile, requirements.txt and app.py in same directory.

2. From that directory, run
  docker build -t friendlyhello .

3. Show the image built in the previous step.
  docker images

4. Run the app, mapping our port 4000 to port 80 of the container.
  docker run -p 4000:80 friendlyhello
  or
  docker run -d -p 4000:80 friendlyhello, detached mode

5. Stop container.
  docker container ls
  docker stop "container_id"

6. Login to Docker Cloud.
  docker Login

7. Upload image to repository
  docker tag image albloptor/get-started:part2

8. Run image and, if it's not available locally, download it from the remote repository
  docker run -p 4000:80 albloptor/get-started:part2

  
